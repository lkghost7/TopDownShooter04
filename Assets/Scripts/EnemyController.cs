﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] int health = 100;
    [SerializeField] float fireRate = 1f;
    [SerializeField] float speed = 1f;
    [SerializeField] Transform shootPosition;
    [SerializeField] GameObject bullet;
    [SerializeField] [Tooltip("Distance when enemy start following player")] float followDistance = 2f;
    [SerializeField] float shootDistance = 1f;
    [SerializeField] float viewAngle = 60;


    PlayerController player;
    EnemyState currentState;
    Coroutine shootingCoroutine;

    Rigidbody2D rigidbody;
    Animator animator;

    enum EnemyState
    {
        STAND,
        MOVE,
        SHOOT
    }

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponentInChildren<Animator>();

        player = FindObjectOfType<PlayerController>();
        currentState = EnemyState.STAND;
    }

    // Update is called once per frame
    void Update()
    {
        //if(currentState == EnemyState.STAND)
        //{
        //    //do STAND
        //}
        //else if(currentState == EnemyState.MOVE)
        //{
        //    //do MOVE
        //}
        //else if(currentState == EnemyState.SHOOT)
        //{
        //    //do SHOOT
        //}
        switch(currentState)
        {
            case EnemyState.STAND:
                //do STAND;
                DoStand();
                break;
            case EnemyState.MOVE:
                //do MOVE;
                DoMove();
                break;
            case EnemyState.SHOOT:
                //do SHOOT;
                DoShoot();
                break;
            default:
                //do default action
                break;
        }

    }

    private void DoStand()
    {
        float distance = Vector2.Distance(transform.position, player.transform.position);
        if (distance <= followDistance)
        {
            Vector2 direction = player.transform.position - transform.position;
            float angle = Vector2.Angle(transform.up, direction); //angle between enemy's look direction and player

            if (angle < viewAngle / 2)
            {
                int layerMask = LayerMask.GetMask("Obstacles");
                RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, distance, layerMask);
                if (hit.collider == null)
                {
                    currentState = EnemyState.MOVE;
                    animator.SetTrigger("Move");
                }
            }

        }
    }
    private void DoMove()
    {
        Move();
        Rotate();
        float distance = Vector2.Distance(transform.position, player.transform.position);
        if (distance <= shootDistance)
        {
            currentState = EnemyState.SHOOT;
            animator.SetTrigger("Shoot");
            StopMovement();
        }
    }

    private void DoShoot()
    {
        Rotate();
        Shoot();

        float distance = Vector2.Distance(transform.position, player.transform.position);
        if (distance > shootDistance)
        {
            currentState = EnemyState.MOVE;
            animator.SetTrigger("Move");
            StopCoroutine(shootingCoroutine);
            shootingCoroutine = null;
        }
    }

    private void Rotate()
    {
        //Vector substraction
        Vector2 direction = player.transform.position - transform.position;
        transform.up = direction;
    }

    private void Move()
    {
        Vector2 direction = player.transform.position - transform.position;
        rigidbody.velocity = direction.normalized * speed;
    }

    private void StopMovement()
    {
        rigidbody.velocity = Vector2.zero;
    }

    private void Shoot()
    {
        if (shootingCoroutine == null)
        {
            shootingCoroutine = StartCoroutine(Shooting());
        }
    }

    IEnumerator Shooting()
    {
        while (true)
        {
            Instantiate(bullet, shootPosition.position, transform.rotation);
            yield return new WaitForSeconds(fireRate);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        DamageDealer damageDealer = collision.GetComponent<DamageDealer>();
        if (damageDealer != null)
        {
            health -= damageDealer.GetDamage();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, followDistance);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, shootDistance);

        Gizmos.color = Color.green;

        Vector3 rotatedVector = Quaternion.AngleAxis(viewAngle / 2, transform.forward) * transform.up * followDistance;
        Vector3 rotatedVector2 = Quaternion.AngleAxis(-viewAngle / 2, transform.forward) * transform.up * followDistance;
        Gizmos.DrawRay(transform.position, rotatedVector);
        Gizmos.DrawRay(transform.position, rotatedVector2);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour{
    [SerializeField] float explodeDelay = 2f;

    [Range(0.4f, 10)] [SerializeField] float explodeRadius = 2f;
    // [SerializeField] private GameObject ExplodeAnim;


    private Animator animator;
    // Start is called before the first frame update

    private void Awake() {
        animator = GetComponent<Animator>();
    }

    void Start() {
        StartCoroutine(Explode());
    }

    IEnumerator Explode() {
        yield return new WaitForSeconds(explodeDelay);
        // Lean.Pool.LeanPool.Spawn(ExplodeAnim, transform.position, Quaternion.identity);
        // Lean.Pool.LeanPool.Despawn(ExplodeAnim);
        // animator.SetTrigger("Start");
        animator.SetInteger("BombInt", 1);
        
        
        // animator.SetBool("st", true);

        StartCoroutine(ExplodCorutine());

        StartCoroutine(Destr());

        // Destroy(gameObject);
        // Lean.Pool.LeanPool.Despawn(gameObject);
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explodeRadius);
    }

    IEnumerator Destr() {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }

    IEnumerator ExplodCorutine() {
        yield return new WaitForSeconds(0.3f);
        int          layerMask        = LayerMask.GetMask("Enemy", "EnemyBullet");
        Collider2D[] objectsToDestroy = Physics2D.OverlapCircleAll(transform.position, explodeRadius, layerMask);
        foreach(Collider2D col in objectsToDestroy) {
            Destroy(col.gameObject);
            // Lean.Pool.LeanPool.Despawn(gameObject);
        }
    }
}
